CREATE TABLE IF NOT EXISTS customers (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  age INT NOT NULL,
  address VARCHAR(255) NOT NULL,

  created_at timestamp default now(),
  updated_at timestamp default now() on update now()
);
CREATE TABLE IF NOT EXISTS manufacturers (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255) NOT NULL
);
CREATE TABLE IF NOT EXISTS models (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  manufacturer INT,

  FOREIGN KEY (manufacturer) REFERENCES manufacturers(id) ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS cars (
  id INT AUTO_INCREMENT PRIMARY KEY,
  year INT NOT NULL,
  model INT,
  owner INT,

  created_at timestamp default now(),
  updated_at timestamp default now() on update now(),

  FOREIGN KEY (owner) REFERENCES customers(id) ON DELETE CASCADE,
  FOREIGN KEY (model) REFERENCES models(id) ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS repairs (
  id INT AUTO_INCREMENT PRIMARY KEY,
  description TEXT NOT NULL,
  price INT NOT NULL,
  car INT,

  created_at timestamp default now(),
  updated_at timestamp default now() on update now(),

  FOREIGN KEY (car) REFERENCES cars(id) ON DELETE CASCADE
);