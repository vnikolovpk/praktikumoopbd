package com.pkdevs.vnikolov.views.models;

import com.pkdevs.vnikolov.database.models.Manufacturer;
import com.pkdevs.vnikolov.database.models.Model;
import com.pkdevs.vnikolov.util.ComboItem;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;

public class ModelsTableModel extends AbstractTableModel {
    private static final String COLUMN_NAME = "Name";
    private static final String COLUMN_MANUFACTURER = "Manufacturer";

    private final List<String> columnNames = Arrays.asList(
            COLUMN_MANUFACTURER, COLUMN_NAME
    );

    private final BiConsumer<Integer, Model> updateHandler;
    private final JTable table;

    private List<Model> dataset;
    private List<Manufacturer> secondaryDataset;

    private JComboBox comboBox = new JComboBox();

    public ModelsTableModel(JTable table, BiConsumer<Integer, Model> updateHandler) {
        this.table = table;
        this.updateHandler = updateHandler;
    }

    public void initComboBox(){
        table.getColumnModel()
                .getColumn(columnNames.indexOf(COLUMN_MANUFACTURER))
                .setCellEditor(new DefaultCellEditor(comboBox));
    }

    public void setDataset(List<Model> dataset) {
        this.dataset = dataset;
        fireTableDataChanged();
    }

    @Override
    public void setValueAt(Object o, int row, int col) {
        if(columnNames.indexOf(COLUMN_NAME) == col) nameUpdated((String) o, dataset.get(row));
        if(columnNames.indexOf(COLUMN_MANUFACTURER) == col) manufacturerUpdated((ComboItem) o, dataset.get(row));
    }

    private void nameUpdated(String newValue, Model current){
        if(!newValue.equals(current.name)){
            updateHandler.accept(
                current.id,
                new Model(
                    null,
                    newValue,
                    null
                )
            );
        }
    }

    private void manufacturerUpdated(ComboItem newValue, Model current) {
        if(newValue.getValue() != current.manufacturer){
            updateHandler.accept(
                current.id,
                new Model(
                    null,
                    null,
                    newValue.getValue()
                )
            );
        }
    }

    @Override
    public boolean isCellEditable(int i, int i1) {
        return true;
    }

    @Override
    public String getColumnName(int i) {
        return columnNames.get(i);
    }

    @Override
    public int getRowCount() {
        return dataset != null ? dataset.size() : 0;
    }

    @Override
    public int getColumnCount() {
        return columnNames.size();
    }

    @Override
    public Class<?> getColumnClass(int col) {
        if(columnNames.indexOf(COLUMN_NAME) == col) return String.class;
        if(columnNames.indexOf(COLUMN_MANUFACTURER) == col) return String.class;
        return null;
    }

    @Override
    public Object getValueAt(int row, int col) {
        if(columnNames.indexOf(COLUMN_NAME) == col) return dataset.get(row).name;
        if(columnNames.indexOf(COLUMN_MANUFACTURER) == col) {
            if(secondaryDataset == null) return null;
            Manufacturer item = secondaryDataset
                    .stream()
                    .filter(i -> i.id.equals(dataset.get(row).manufacturer))
                    .findAny()
                    .orElse(null);
            return item != null ? item.name : null;
        }
        return null;
    }

    public Model getItemAtRow(int row){
        return dataset.get(row);
    }

    public void setSecondaryDataset(List<Manufacturer> secondaryDataset) {
        this.secondaryDataset = secondaryDataset;
        comboBox.removeAllItems();
        for (Manufacturer item : secondaryDataset) {
            comboBox.addItem(new ComboItem(item.id, item.name));
        }
        fireTableDataChanged();
    }
}
