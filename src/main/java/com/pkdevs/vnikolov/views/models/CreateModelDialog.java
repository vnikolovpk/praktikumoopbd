package com.pkdevs.vnikolov.views.models;

import com.pkdevs.vnikolov.database.models.Manufacturer;
import com.pkdevs.vnikolov.database.models.Model;
import com.pkdevs.vnikolov.util.ComboItem;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import java.util.function.Consumer;

public class CreateModelDialog extends JDialog {
    private final Consumer<Model> onSuccessListener;

    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;

    private JTextField etName;

    private JComboBox cbManufacturer;

    private JLabel tvError;

    public CreateModelDialog(Consumer<Model> onSuccessListener, List<Manufacturer> secondaryDataset) {
        this.onSuccessListener = onSuccessListener;

        for (Manufacturer item : secondaryDataset) {
            cbManufacturer.addItem(new ComboItem(item.id, item.name));
        }

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());
        buttonCancel.addActionListener(e -> onCancel());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void onOK() {
        ComboItem secondaryChoice;

        // add your code here
        if(!etName.isValid() || etName.getText().isEmpty()){
            tvError.setText("Please enter name");
            return;
        }

        secondaryChoice = (ComboItem) cbManufacturer.getSelectedItem();

        onSuccessListener.accept(new Model(
                null,
                etName.getText(),
                secondaryChoice.getValue()
        ));
        dispose();
    }

    private void onCancel() {
        dispose();
    }
}
