package com.pkdevs.vnikolov.views.models;

import com.pkdevs.vnikolov.database.Database;
import com.pkdevs.vnikolov.database.models.Manufacturer;
import com.pkdevs.vnikolov.database.models.Model;
import com.pkdevs.vnikolov.util.PlaceholderSearchFIeld;
import com.pkdevs.vnikolov.util.TableMouseListener;
import com.pkdevs.vnikolov.views.CrudPopupMenu;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;

public class ModelsPage extends ComponentAdapter {
    private JPanel rootPanel;
    private JButton createButton;
    private JPanel tablePanel;
    private JTextField searchField;

    // Dependencies
    private final Database database;

    private final ModelsTableModel tableModel;

    private List<Manufacturer> secondaryDataset;

    public ModelsPage(Database database) {
        this.database = database;

        rootPanel.addComponentListener(this);
        tablePanel.setLayout(new BorderLayout());

        JTable table = new JTable();

        tableModel = new ModelsTableModel(table, this::update);

        CrudPopupMenu crudPopupMenu = new CrudPopupMenu(
                table,
                i -> delete(tableModel.getItemAtRow(i))
        );

        table.setComponentPopupMenu(crudPopupMenu);

        table.addMouseListener(new TableMouseListener(table));

        table.setModel(tableModel);
        table.setFillsViewportHeight(true);

        tableModel.initComboBox();

        tablePanel.add(table.getTableHeader(), BorderLayout.PAGE_START);
        tablePanel.add(table, BorderLayout.CENTER);

        createButton.addActionListener(this::createButtonClicked);
    }

    private void createButtonClicked(ActionEvent actionEvent) {
        CreateModelDialog dialog = new CreateModelDialog(
                this::createRepair, secondaryDataset
        );
        dialog.pack();
        dialog.setVisible(true);
    }

    private void createRepair(Model record) {
        database.models.insert(record);
        refreshModels();
    }

    private void update(Integer id, Model updates) {
        database.models.update(id, updates);
        refreshModels();
    }

    public JPanel getPanel() {
        return rootPanel;
    }

    @Override
    public void componentShown(ComponentEvent componentEvent) {
        refreshModels();
        refreshManufacturers();
    }

    public void refreshModels(){
        refreshModels(null);
    }
    public void refreshModels(String q){
        setDataset(database.models.list(q));
    }
    public void refreshManufacturers() { setSecondaryDataset(database.manufacturers.list()); }

    private void setSecondaryDataset(List<Manufacturer> setcondaryDataset) {
        this.secondaryDataset = setcondaryDataset;
        tableModel.setSecondaryDataset(setcondaryDataset);
    }

    public void setDataset(List<Model> dataset) {
        tableModel.setDataset(dataset);
    }

    private void delete(Model record){
        database.models.delete(record.id);
        refreshModels();
    }

    private void createUIComponents() {
        searchField = new PlaceholderSearchFIeld();
        searchField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent keyEvent) {
                String text = searchField.getText();
                refreshModels(text.isEmpty() ? null : text);
            }
        });
    }
}
