package com.pkdevs.vnikolov.views.manufacturers;

import com.pkdevs.vnikolov.database.Database;
import com.pkdevs.vnikolov.database.models.Manufacturer;
import com.pkdevs.vnikolov.util.PlaceholderSearchFIeld;
import com.pkdevs.vnikolov.util.TableMouseListener;
import com.pkdevs.vnikolov.views.CrudPopupMenu;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;

public class ManufacturersPage extends ComponentAdapter {
    private JPanel rootPanel;
    private JButton createButton;
    private JPanel tablePanel;
    private JTextField searchField;

    // Dependencies
    private final Database database;

    private final ManufacturersTableModel tableModel;

    public ManufacturersPage(Database database) {
        this.database = database;

        rootPanel.addComponentListener(this);
        tablePanel.setLayout(new BorderLayout());

        JTable table = new JTable();

        tableModel = new ManufacturersTableModel(table, this::update);

        CrudPopupMenu crudPopupMenu = new CrudPopupMenu(
                table,
                i -> deleteManufacturer(tableModel.getManufacturerAtRow(i))
        );

        table.setComponentPopupMenu(crudPopupMenu);

        table.addMouseListener(new TableMouseListener(table));

        table.setModel(tableModel);
        table.setFillsViewportHeight(true);

        tablePanel.add(table.getTableHeader(), BorderLayout.PAGE_START);
        tablePanel.add(table, BorderLayout.CENTER);

        createButton.addActionListener(this::createButtonClicked);
    }

    private void createButtonClicked(ActionEvent actionEvent) {
        CreateManufacturerDialog dialog = new CreateManufacturerDialog(this::create);
        dialog.pack();
        dialog.setVisible(true);
    }

    private void create(Manufacturer car) {
        database.manufacturers.insert(car);
        refreshManufacturers();
    }

    private void update(Integer id, Manufacturer updates) {
        database.manufacturers.update(id, updates);
        refreshManufacturers();
    }

    public JPanel getPanel() {
        return rootPanel;
    }

    @Override
    public void componentShown(ComponentEvent componentEvent) {
        refreshManufacturers();
    }

    public void refreshManufacturers(){
        refreshManufacturers(null);
    }
    public void refreshManufacturers(String query){
        setManufacturers(database.manufacturers.list(query));
    }


    public void setManufacturers(List<Manufacturer> cars) {
        tableModel.setManufacturers(cars);
    }

    private void deleteManufacturer(Manufacturer record){
        database.manufacturers.delete(record.id);
        refreshManufacturers();
    }

    private void createUIComponents() {
        searchField = new PlaceholderSearchFIeld();
        searchField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent keyEvent) {
                String text = searchField.getText();
                refreshManufacturers(text.isEmpty() ? null : text);
            }
        });
    }
}
