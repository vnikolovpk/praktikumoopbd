package com.pkdevs.vnikolov.views.manufacturers;

import com.pkdevs.vnikolov.database.models.Manufacturer;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.Collections;
import java.util.List;
import java.util.function.BiConsumer;

public class ManufacturersTableModel extends AbstractTableModel {
    private static final String COLUMN_NAME = "Name";

    private final List<String> columnNames = Collections.singletonList(
            COLUMN_NAME
    );

    private final BiConsumer<Integer, Manufacturer> updateHandler;
    private final JTable table;

    private List<Manufacturer> dataset;

    public ManufacturersTableModel(JTable table, BiConsumer<Integer, Manufacturer> updateHandler) {
        this.table = table;
        this.updateHandler = updateHandler;
    }

    public void setManufacturers(List<Manufacturer> dataset) {
        this.dataset = dataset;
        fireTableDataChanged();
    }

    @Override
    public void setValueAt(Object o, int row, int col) {
        if(columnNames.indexOf(COLUMN_NAME) == col) nameUpdated((String) o, dataset.get(row));
    }

    private void nameUpdated(String newValue, Manufacturer current){
        if(!newValue.equals(current.name)){
            updateHandler.accept(
                    current.id,
                    new Manufacturer(
                            null,
                            newValue
                    )
            );
        }
    }

    @Override
    public boolean isCellEditable(int i, int i1) {
        return true;
    }

    @Override
    public String getColumnName(int i) {
        return columnNames.get(i);
    }

    @Override
    public int getRowCount() {
        return dataset != null ? dataset.size() : 0;
    }

    @Override
    public int getColumnCount() {
        return columnNames.size();
    }

    @Override
    public Class<?> getColumnClass(int col) {
        if(columnNames.indexOf(COLUMN_NAME) == col) return String.class;
        return null;
    }

    @Override
    public Object getValueAt(int row, int col) {
        if(columnNames.indexOf(COLUMN_NAME) == col) return dataset.get(row).name;
        return null;
    }

    public Manufacturer getManufacturerAtRow(int row){
        return dataset.get(row);
    }
}
