package com.pkdevs.vnikolov.views.manufacturers;

import com.pkdevs.vnikolov.database.models.Manufacturer;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.function.Consumer;

public class CreateManufacturerDialog extends JDialog {
    private final Consumer<Manufacturer> onSuccessListener;

    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;

    private JLabel tvError;
    private JTextField etName;

    public CreateManufacturerDialog(Consumer<Manufacturer> onSuccessListener) {
        this.onSuccessListener = onSuccessListener;

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());
        buttonCancel.addActionListener(e -> onCancel());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void onOK() {
        if(!etName.isValid() || etName.getText().isEmpty()){
            tvError.setText("Please enter price");
            return;
        }

        onSuccessListener.accept(new Manufacturer(
                null,
                etName.getText()
        ));
        dispose();
    }

    private void onCancel() {
        dispose();
    }
}
