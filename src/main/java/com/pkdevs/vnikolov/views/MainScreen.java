package com.pkdevs.vnikolov.views;

import com.pkdevs.vnikolov.database.Database;
import com.pkdevs.vnikolov.views.cars.CarsPage;
import com.pkdevs.vnikolov.views.customers.CustomersPage;
import com.pkdevs.vnikolov.views.manufacturers.ManufacturersPage;
import com.pkdevs.vnikolov.views.models.ModelsPage;
import com.pkdevs.vnikolov.views.multisearch.FullRepairsPage;
import com.pkdevs.vnikolov.views.repairs.RepairsPage;

import javax.swing.*;

public class MainScreen extends JFrame {
    // UI components
    private JPanel panel;

    private JPanel carsPanel;
    private JPanel customersPanel;
    private JPanel repairsPanel;
    private JPanel manufacturersPanel;
    private JPanel modelsPanel;
    private JPanel advancedSearchPanel;

    // Dependencies
    private final Database database;

    public MainScreen(Database database) {
        this.database = database;
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        initMainPanel();
        setVisible(true);
        pack();
    }

    private void initMainPanel() {
        add(panel);
    }

    private void createUIComponents() {
        carsPanel = new CarsPage(database).getPanel();
        customersPanel = new CustomersPage(database).getPanel();
        repairsPanel = new RepairsPage(database).getPanel();
        manufacturersPanel = new ManufacturersPage(database).getPanel();
        modelsPanel = new ModelsPage(database).getPanel();
        advancedSearchPanel = new FullRepairsPage(database).getPanel();
    }
}
