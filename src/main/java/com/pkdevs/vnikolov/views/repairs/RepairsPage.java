package com.pkdevs.vnikolov.views.repairs;

import com.pkdevs.vnikolov.database.Database;
import com.pkdevs.vnikolov.database.models.Car;
import com.pkdevs.vnikolov.database.models.Manufacturer;
import com.pkdevs.vnikolov.database.models.Model;
import com.pkdevs.vnikolov.database.models.Repair;
import com.pkdevs.vnikolov.util.PlaceholderSearchFIeld;
import com.pkdevs.vnikolov.util.TableMouseListener;
import com.pkdevs.vnikolov.views.CrudPopupMenu;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;

public class RepairsPage extends ComponentAdapter {
    private JPanel rootPanel;
    private JButton createButton;
    private JPanel tablePanel;
    private JTextField searchField;

    // Dependencies
    private final Database database;

    private final RepairsTableModel tableModel;

    private List<Car> cars;
    private List<Model> models;
    private List<Manufacturer> manufacturers;

    public RepairsPage(Database database) {
        this.database = database;

        rootPanel.addComponentListener(this);
        tablePanel.setLayout(new BorderLayout());

        JTable table = new JTable();

        tableModel = new RepairsTableModel(table, this::updateRepair);

        CrudPopupMenu crudPopupMenu = new CrudPopupMenu(
                table,
                i -> deleteRepair(tableModel.getRepairAtRow(i))
        );

        table.setComponentPopupMenu(crudPopupMenu);

        table.addMouseListener(new TableMouseListener(table));

        table.setModel(tableModel);
        table.setFillsViewportHeight(true);

        tableModel.initComboBox();

        tablePanel.add(table.getTableHeader(), BorderLayout.PAGE_START);
        tablePanel.add(table, BorderLayout.CENTER);

        createButton.addActionListener(this::createButtonClicked);
    }

    private void createButtonClicked(ActionEvent actionEvent) {
        CreateRepairDialog dialog = new CreateRepairDialog(
                this::createRepair, cars, manufacturers, models
        );
        dialog.pack();
        dialog.setVisible(true);
    }

    private void createRepair(Repair car) {
        database.repairs.insert(car);
        refreshRepairs();
    }

    private void updateRepair(Integer id, Repair updates) {
        database.repairs.update(id, updates);
        refreshRepairs();
    }

    public JPanel getPanel() {
        return rootPanel;
    }

    @Override
    public void componentShown(ComponentEvent componentEvent) {
        // The order here matters xD because good code thats why
        refreshManufacturers();
        refreshModels();
        refreshCars();
        refreshRepairs();
    }

    public void refreshRepairs(){
        refreshRepairs(null);
    }
    public void refreshRepairs(String query){
        setRepairs(database.repairs.list(query));
    }

    public void refreshCars() { setCars(database.cars.list()); }
    public void refreshModels() { setModels(database.models.list()); }
    public void refreshManufacturers() { setManufacturers(database.manufacturers.list()); }

    private void setCars(List<Car> cars) {
        this.cars = cars;
        tableModel.setCars(cars);
    }
    private void setModels(List<Model> models) {
        this.models = models;
        tableModel.setModels(models);
    }
    private void setManufacturers(List<Manufacturer> manufacturers) {
        this.manufacturers = manufacturers;
        tableModel.setManufacturers(manufacturers);
    }

    public void setRepairs(List<Repair> cars) {
        tableModel.setRepairs(cars);
    }

    private void deleteRepair(Repair repair){
        database.repairs.delete(repair.id);
        refreshRepairs();
    }

    private void createUIComponents() {
        searchField = new PlaceholderSearchFIeld();
        searchField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent keyEvent) {
                String text = searchField.getText();
                refreshRepairs(text.isEmpty() ? null : text);
            }
        });
    }
}
