package com.pkdevs.vnikolov.views.repairs;

import com.pkdevs.vnikolov.database.models.Car;
import com.pkdevs.vnikolov.database.models.Manufacturer;
import com.pkdevs.vnikolov.database.models.Model;
import com.pkdevs.vnikolov.database.models.Repair;
import com.pkdevs.vnikolov.util.ComboItem;
import com.pkdevs.vnikolov.util.Relator;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;

public class RepairsTableModel extends AbstractTableModel {
    private static final String COLUMN_DESCRIPTION = "Description";
    private static final String COLUMN_PRICE = "Price";
    private static final String COLUMN_CAR = "Car";

    private final List<String> columnNames = Arrays.asList(
            COLUMN_DESCRIPTION, COLUMN_PRICE, COLUMN_CAR
    );
    private final BiConsumer<Integer, Repair> updateHandler;
    private final JTable table;

    private List<Repair> repairs;
    private List<Car> cars;
    private List<Model> models;
    private List<Manufacturer> manufacturers;

    private JComboBox comboBox = new JComboBox();

    public RepairsTableModel(JTable table, BiConsumer<Integer, Repair> updateHandler) {
        this.table = table;
        this.updateHandler = updateHandler;
    }

    public void initComboBox(){
        table.getColumnModel()
            .getColumn(columnNames.indexOf(COLUMN_CAR))
            .setCellEditor(new DefaultCellEditor(comboBox));
    }

    public void setRepairs(List<Repair> repairs) {
        this.repairs = repairs;
        fireTableDataChanged();
    }

    public void setManufacturers(List<Manufacturer> manufacturers) {
        this.manufacturers = manufacturers;
        fireTableDataChanged();
    }

    public void setModels(List<Model> models) {
        this.models = models;
        fireTableDataChanged();
    }

    @Override
    public void setValueAt(Object o, int row, int col) {
        if(columnNames.indexOf(COLUMN_DESCRIPTION) == col) descriptionUpdated((String) o, repairs.get(row));
        if(columnNames.indexOf(COLUMN_PRICE) == col) priceUpdated((int) o, repairs.get(row));
        if(columnNames.indexOf(COLUMN_CAR) == col) carUpdated((ComboItem) o, repairs.get(row));
    }

    private void descriptionUpdated(String newValue, Repair current){
        if(!newValue.equals(current.description)){
            updateHandler.accept(
                current.id,
                new Repair(
                    null,
                    newValue,
                    null,
                    null,
                    null,
                    null
                )
            );
        }
    }

    private void priceUpdated(int newValue, Repair current){
        if(newValue != current.price){
            updateHandler.accept(
                current.id,
                new Repair(
                    null,
                    null,
                    newValue,
                    null,
                    null,
                    null
                )
            );
        }
    }

    private void carUpdated(ComboItem newValue, Repair current) {
        if(newValue.getValue() != current.car){
            updateHandler.accept(
                current.id,
                new Repair(
                    null,
                    null,
                    null,
                    newValue.getValue(),
                    null,
                    null
                )
            );
        }
    }

    @Override
    public boolean isCellEditable(int i, int i1) {
        return true;
    }

    @Override
    public String getColumnName(int i) {
        return columnNames.get(i);
    }

    @Override
    public int getRowCount() {
        return repairs != null ? repairs.size() : 0;
    }

    @Override
    public int getColumnCount() {
        return columnNames.size();
    }

    @Override
    public Class<?> getColumnClass(int col) {
        if(columnNames.indexOf(COLUMN_DESCRIPTION) == col) return String.class;
        if(columnNames.indexOf(COLUMN_PRICE) == col) return Integer.class;
        if(columnNames.indexOf(COLUMN_CAR) == col) return String.class;
        return null;
    }

    @Override
    public Object getValueAt(int row, int col) {
        if(columnNames.indexOf(COLUMN_DESCRIPTION) == col) return repairs.get(row).description;
        if(columnNames.indexOf(COLUMN_PRICE) == col) return repairs.get(row).price;
        if(columnNames.indexOf(COLUMN_CAR) == col) {
            if(cars == null) return null;
            Car car = cars
                    .stream()
                    .filter(i -> i.id.equals(repairs.get(row).car))
                    .findAny()
                    .orElse(null);
            return car != null ? makeModelForCar(car) : null;
        }
        return null;
    }

    public Repair getRepairAtRow(int row){
        return repairs.get(row);
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
        comboBox.removeAllItems();
        for (Car car : cars) {
            comboBox.addItem(new ComboItem(car.id, makeModelForCar(car)));
        }
        fireTableDataChanged();
    }

    private String makeModelForCar(Car car){
        return Relator.makeModelForCar(manufacturers, models, car);
    }
}
