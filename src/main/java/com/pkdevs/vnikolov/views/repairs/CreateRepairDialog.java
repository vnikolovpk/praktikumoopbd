package com.pkdevs.vnikolov.views.repairs;

import com.pkdevs.vnikolov.database.models.*;
import com.pkdevs.vnikolov.util.ComboItem;
import com.pkdevs.vnikolov.util.Relator;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import java.util.function.Consumer;

public class CreateRepairDialog extends JDialog {
    private final Consumer<Repair> onSuccessListener;

    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;

    private JTextField etPrice;
    private JTextArea taDescription;

    private JComboBox cbCar;

    private JLabel tvError;

    public CreateRepairDialog(
            Consumer<Repair> onSuccessListener,
            List<Car> cars,
            List<Manufacturer> manufacturers,
            List<Model> models) {
        this.onSuccessListener = onSuccessListener;

        for (Car car : cars) {
            cbCar.addItem(new ComboItem(car.id, Relator.makeModelForCar(manufacturers, models, car)));
        }

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());
        buttonCancel.addActionListener(e -> onCancel());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void onOK() {
        int price;

        String description;

        ComboItem car;

        // add your code here
        if(!etPrice.isValid() || etPrice.getText().isEmpty()){
            tvError.setText("Please enter price");
            return;
        }
        if(!taDescription.isValid() || taDescription.getText().isEmpty()){
            tvError.setText("Please enter year");
            return;
        }

        description = taDescription.getText();

        try {
            price = Integer.parseInt(etPrice.getText());
        } catch (NumberFormatException e) {
            tvError.setText("Invalid year entered...");
            return;
        }

        car = (ComboItem) cbCar.getSelectedItem();

        onSuccessListener.accept(new Repair(
                null,
                description,
                price,
                car.getValue(),
                null,
                null
        ));
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }
}
