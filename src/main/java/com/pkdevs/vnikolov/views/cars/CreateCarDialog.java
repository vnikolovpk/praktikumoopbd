package com.pkdevs.vnikolov.views.cars;

import com.pkdevs.vnikolov.database.models.Car;
import com.pkdevs.vnikolov.database.models.Customer;
import com.pkdevs.vnikolov.database.models.Model;
import com.pkdevs.vnikolov.util.ComboItem;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import java.util.function.Consumer;

public class CreateCarDialog extends JDialog {
    private final Consumer<Car> onSuccessListener;

    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;

    private JTextField etYear;
    private JComboBox cbOwner;
    private JComboBox cbModel;

    private JLabel tvError;

    public CreateCarDialog(Consumer<Car> onSuccessListener,
                           List<Customer> customers,
                           List<Model> models) {
        this.onSuccessListener = onSuccessListener;

        for (Customer customer : customers) {
            cbOwner.addItem(new ComboItem(customer.id, customer.name));
        }

        for (Model model : models) {
            cbModel.addItem(new ComboItem(model.id, model.name));
        }

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());
        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void onOK() {
        int year;
        ComboItem owner;
        ComboItem model;

        if(!etYear.isValid() || etYear.getText().isEmpty()){
            tvError.setText("Please enter year");
            return;
        }

        try {
            year = Integer.parseInt(etYear.getText());
        } catch (NumberFormatException e) {
            tvError.setText("Invalid year entered...");
            return;
        }

        owner = (ComboItem) cbOwner.getSelectedItem();
        model = (ComboItem) cbModel.getSelectedItem();

        onSuccessListener.accept(new Car(
                null,
                model.getValue(),
                year,
                owner.getValue(),
                null,
                null
        ));
        dispose();
    }

    private void onCancel() {
        dispose();
    }
}
