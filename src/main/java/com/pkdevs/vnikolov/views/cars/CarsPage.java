package com.pkdevs.vnikolov.views.cars;

import com.pkdevs.vnikolov.database.Database;
import com.pkdevs.vnikolov.database.models.Car;
import com.pkdevs.vnikolov.database.models.Customer;
import com.pkdevs.vnikolov.database.models.Model;
import com.pkdevs.vnikolov.util.PlaceholderSearchFIeld;
import com.pkdevs.vnikolov.util.TableMouseListener;
import com.pkdevs.vnikolov.views.CrudPopupMenu;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;

public class CarsPage extends ComponentAdapter {
    private JPanel rootPanel;
    private JButton createButton;
    private JPanel tablePanel;
    private JTextField searchField;

    // Dependencies
    private final Database database;

    private final CarsTableModel carsTableModel;
    private List<Customer> customers;
    private List<Model> models;

    public CarsPage(Database database) {
        this.database = database;

        rootPanel.addComponentListener(this);
        tablePanel.setLayout(new BorderLayout());

        JTable table = new JTable();

        carsTableModel = new CarsTableModel(table, this::updateCar);

        CrudPopupMenu crudPopupMenu = new CrudPopupMenu(
                table,
                i -> deleteCar(carsTableModel.getCarAtRow(i))
        );

        table.setComponentPopupMenu(crudPopupMenu);

        table.addMouseListener(new TableMouseListener(table));

        table.setModel(carsTableModel);
        table.setFillsViewportHeight(true);

        carsTableModel.initComboBoxes();

        tablePanel.add(table.getTableHeader(), BorderLayout.PAGE_START);
        tablePanel.add(table, BorderLayout.CENTER);

        createButton.addActionListener(this::createButtonClicked);
    }

    private void createButtonClicked(ActionEvent actionEvent) {
        CreateCarDialog dialog = new CreateCarDialog(
                this::createCar, customers, models
        );
        dialog.pack();
        dialog.setVisible(true);
    }

    private void createCar(Car car) {
        database.cars.insert(car);
        refreshCars();
    }

    private void updateCar(Integer id, Car updates) {
        database.cars.update(id, updates);
        refreshCars();
    }

    public JPanel getPanel() {
        return rootPanel;
    }

    @Override
    public void componentShown(ComponentEvent componentEvent) {
        refreshCars();
        refreshCustomers();
        refreshModels();
    }

    public void refreshCars(){refreshCars(null);}
    public void refreshCars(String search){
        setCars(database.cars.list(search));
    }
    public void refreshCustomers() {
        setCustomers(database.customers.list());
    }
    public void refreshModels() { setModels(database.models.list()); }

    private void setCustomers(List<Customer> customers) {
        this.customers = customers;
        carsTableModel.setCustomers(customers);
    }

    private void setModels(List<Model> models) {
        this.models = models;
        carsTableModel.setModels(models);
    }

    public void setCars(List<Car> cars) {
        carsTableModel.setCars(cars);
    }

    private void deleteCar(Car car){
        database.cars.delete(car.id);
        refreshCars();
    }

    private void createUIComponents() {
        searchField = new PlaceholderSearchFIeld();
        searchField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent keyEvent) {
                String text = searchField.getText();
                refreshCars(text.isEmpty() ? null : text);
            }
        });
    }
}
