package com.pkdevs.vnikolov.views.cars;

import com.pkdevs.vnikolov.database.models.Car;
import com.pkdevs.vnikolov.database.models.Customer;
import com.pkdevs.vnikolov.database.models.Model;
import com.pkdevs.vnikolov.util.ComboItem;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;

public class CarsTableModel extends AbstractTableModel {

    private static final String COLUMN_MODEL = "Model";
    private static final String COLUMN_YEAR = "Year";
    private static final String COLUMN_OWNER = "Owner";

    private final List<String> columnNames = Arrays.asList(
            COLUMN_MODEL, COLUMN_YEAR, COLUMN_OWNER
    );
    private final JTable table;
    private final BiConsumer<Integer, Car> carUpdatedHandler;

    private List<Car> cars;
    private List<Customer> customers;
    private List<Model> models;

    private JComboBox cbCustomers = new JComboBox();
    private JComboBox cbModels = new JComboBox();

    public CarsTableModel(JTable table, BiConsumer<Integer, Car> carUpdatedHandler) {
        this.table = table;
        this.carUpdatedHandler = carUpdatedHandler;
    }

    public void initComboBoxes(){
        table.getColumnModel()
            .getColumn(columnNames.indexOf(COLUMN_OWNER))
            .setCellEditor(new DefaultCellEditor(cbCustomers));
        table.getColumnModel()
            .getColumn(columnNames.indexOf(COLUMN_MODEL))
            .setCellEditor(new DefaultCellEditor(cbModels));
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
        fireTableDataChanged();
    }

    @Override
    public void setValueAt(Object o, int row, int col) {
        if(columnNames.indexOf(COLUMN_MODEL) == col) modelUpdated((ComboItem) o, cars.get(row));
        if(columnNames.indexOf(COLUMN_OWNER) == col) ownerUpdated((ComboItem) o, cars.get(row));
        if(columnNames.indexOf(COLUMN_YEAR) == col) yearUpdated((int) o, cars.get(row));
    }

    private void yearUpdated(int newValue, Car current){
        if(newValue != current.year){
            carUpdatedHandler.accept(
                current.id,
                new Car(
                    null,
                    null,
                    newValue,
                    null,
                    null,
                    null
                )
            );
        }
    }

    private void ownerUpdated(ComboItem newValue, Car current) {
        if(newValue.getValue() != current.owner){
            carUpdatedHandler.accept(
                current.id,
                new Car(
                    null,
                    null,
                    null,
                    newValue.getValue(),
                    null,
                    null
                )
            );
        }
    }

    private void modelUpdated(ComboItem newValue, Car current) {
        System.out.println("NEMA BIZNIES");
        if(newValue.getValue() != current.model){
            carUpdatedHandler.accept(
                current.id,
                new Car(
                    null,
                    newValue.getValue(),
                    null,
                    null,
                    null,
                    null
                )
            );
        }
    }

    @Override
    public boolean isCellEditable(int i, int i1) {
        return true;
    }

    @Override
    public String getColumnName(int i) {
        return columnNames.get(i);
    }

    @Override
    public int getRowCount() {
        return cars != null ? cars.size() : 0;
    }

    @Override
    public int getColumnCount() {
        return columnNames.size();
    }

    @Override
    public Class<?> getColumnClass(int col) {
        if(columnNames.indexOf(COLUMN_MODEL) == col) return String.class;
        if(columnNames.indexOf(COLUMN_YEAR) == col) return Integer.class;
        if(columnNames.indexOf(COLUMN_OWNER) == col) return String.class;
        return null;
    }

    @Override
    public Object getValueAt(int row, int col) {
        if(columnNames.indexOf(COLUMN_YEAR) == col) return cars.get(row).year;
        if(columnNames.indexOf(COLUMN_OWNER) == col) {
            if(customers == null) return null;
            Customer customer = customers
                    .stream()
                    .filter(i -> i.id.equals(cars.get(row).owner))
                    .findAny()
                    .orElse(null);
            return customer != null ? customer.name : null;
        }
        if(columnNames.indexOf(COLUMN_MODEL) == col) {
            if(models == null) return null;
            Model model = models
                    .stream()
                    .filter(i -> i.id.equals(cars.get(row).model))
                    .findAny()
                    .orElse(null);
            return model != null ? model.name : null;
        }
        return null;
    }

    public Car getCarAtRow(int row){
        return cars.get(row);
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
        cbCustomers.removeAllItems();
        for (Customer customer : customers) {
            cbCustomers.addItem(new ComboItem(customer.id, customer.name));
        }
        fireTableDataChanged();
    }

    public void setModels(List<Model> models) {
        this.models = models;
        cbModels.removeAllItems();
        for (Model model : models) {
            cbModels.addItem(new ComboItem(model.id, model.name));
        }
        fireTableDataChanged();
    }
}
