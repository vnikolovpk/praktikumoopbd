package com.pkdevs.vnikolov.views.multisearch;

import com.pkdevs.vnikolov.database.models.*;
import com.pkdevs.vnikolov.util.ComboItem;
import com.pkdevs.vnikolov.util.Relator;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;

public class FullRepairTableModel extends AbstractTableModel {
    private static final String COLUMN_DESCRIPTION = "Description";
    private static final String COLUMN_PRICE = "Price";
    private static final String COLUMN_CUSTOMER = "Customer";
    private static final String COLUMN_CAR = "Car";

    private final List<String> columnNames = Arrays.asList(
            COLUMN_DESCRIPTION, COLUMN_PRICE, COLUMN_CAR, COLUMN_CUSTOMER
    );

    private List<FullRepair> dataset;

    public FullRepairTableModel() {}

    @Override
    public boolean isCellEditable(int i, int i1) {
        return false;
    }

    @Override
    public String getColumnName(int i) {
        return columnNames.get(i);
    }

    @Override
    public int getRowCount() {
        return dataset != null ? dataset.size() : 0;
    }

    @Override
    public int getColumnCount() {
        return columnNames.size();
    }

    @Override
    public Class<?> getColumnClass(int col) {
        if(columnNames.indexOf(COLUMN_DESCRIPTION) == col) return String.class;
        if(columnNames.indexOf(COLUMN_CUSTOMER) == col) return String.class;
        if(columnNames.indexOf(COLUMN_PRICE) == col) return Integer.class;
        if(columnNames.indexOf(COLUMN_CAR) == col) return String.class;
        return null;
    }

    @Override
    public Object getValueAt(int row, int col) {
        if(columnNames.indexOf(COLUMN_DESCRIPTION) == col) return dataset.get(row).repair;
        if(columnNames.indexOf(COLUMN_CUSTOMER) == col) return dataset.get(row).customer;
        if(columnNames.indexOf(COLUMN_PRICE) == col) return dataset.get(row).price;
        if(columnNames.indexOf(COLUMN_CAR) == col) return dataset.get(row).car;
        return null;
    }

    public void setDataset(List<FullRepair> dataset) {
        this.dataset = dataset;
        fireTableDataChanged();
    }
}
