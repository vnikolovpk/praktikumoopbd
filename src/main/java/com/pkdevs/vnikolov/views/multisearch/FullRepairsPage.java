package com.pkdevs.vnikolov.views.multisearch;

import com.pkdevs.vnikolov.database.Database;
import com.pkdevs.vnikolov.util.PlaceholderSearchFIeld;
import com.pkdevs.vnikolov.util.TableMouseListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.HashMap;

public class FullRepairsPage extends ComponentAdapter {
    private JPanel rootPanel;
    private JPanel tablePanel;

    private JTextField searchCustomer;
    private JTextField searchRepair;
    private JTextField searchCar;

    // Dependencies
    private final Database database;

    private final FullRepairTableModel tableModel;

    public FullRepairsPage(Database database) {
        this.database = database;

        rootPanel.addComponentListener(this);
        tablePanel.setLayout(new BorderLayout());

        JTable table = new JTable();

        tableModel = new FullRepairTableModel();

        table.addMouseListener(new TableMouseListener(table));

        table.setModel(tableModel);
        table.setFillsViewportHeight(true);

        tablePanel.add(table.getTableHeader(), BorderLayout.PAGE_START);
        tablePanel.add(table, BorderLayout.CENTER);
    }

    public JPanel getPanel() {
        return rootPanel;
    }

    @Override
    public void componentShown(ComponentEvent componentEvent) {
        // The order here matters xD because good code thats why
        refreshDataset();
    }

    private void refreshDataset() {
        HashMap<String, String> filters = new HashMap<>();

        if(!searchCustomer.getText().isEmpty() && !searchCustomer.getText().equals("Search")){
            filters.put("customers.name", searchCustomer.getText());
        }
        if(!searchRepair.getText().isEmpty() && !searchRepair.getText().equals("Search")){
            filters.put("repairs.description", searchRepair.getText());
        }
        if(!searchCar.getText().isEmpty() && !searchCar.getText().equals("Search")){
            filters.put("models.name", searchCar.getText());
        }

        tableModel.setDataset(database.multiTableRepairsSearch(filters));
    }

    private void createUIComponents() {
        KeyAdapter keyAdapter = new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent keyEvent) {
                refreshDataset();
            }
        };

        searchCustomer = new PlaceholderSearchFIeld();
        searchCustomer.addKeyListener(keyAdapter);

        searchRepair = new PlaceholderSearchFIeld();
        searchRepair.addKeyListener(keyAdapter);

        searchCar = new PlaceholderSearchFIeld();
        searchCar.addKeyListener(keyAdapter);
    }
}
