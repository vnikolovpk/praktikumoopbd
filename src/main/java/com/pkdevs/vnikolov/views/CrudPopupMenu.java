package com.pkdevs.vnikolov.views;

import javax.swing.*;
import java.util.function.Consumer;

public class CrudPopupMenu extends JPopupMenu {
    // Buttons
    private final JMenuItem itemDelete = new JMenuItem("Delete record");

    // Event handlers
    private final Consumer<Integer> onDeleteClicked;

    // Dependencies
    private final JTable table;

    public CrudPopupMenu(JTable table, Consumer<Integer> onDeleteClicked) {
        this.table = table;
        this.onDeleteClicked = onDeleteClicked;
        addActionListeners();
        addButtons();
    }

    private void addButtons() {
        add(itemDelete);
    }

    private void addActionListeners() {
        itemDelete.addActionListener(actionEvent -> {
            int selectedRow = table.getSelectedRow();
            if (onDeleteClicked != null && selectedRow >= 0) {
                onDeleteClicked.accept(selectedRow);
            }
        });
    }
}
