package com.pkdevs.vnikolov.views.customers;

import com.pkdevs.vnikolov.database.models.Customer;

import javax.swing.*;
import java.awt.event.*;
import java.util.function.Consumer;

public class CreateCustomerDialog extends JDialog {
    private final Consumer<Customer> onSuccessListener;
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;

    private JTextField etName;
    private JTextField etAge;
    private JTextField etAddress;

    private JLabel tvError;

    public CreateCustomerDialog(Consumer<Customer> onSuccessListener) {
        this.onSuccessListener = onSuccessListener;

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());
        buttonCancel.addActionListener(e -> onCancel());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void onOK() {
        int age;
        String name;
        String address;

        // add your code here
        if(!etName.isValid() || etName.getText().isEmpty()){
            tvError.setText("Please enter name");
            return;
        }
        if(!etAddress.isValid() || etAddress.getText().isEmpty()){
            tvError.setText("Please enter address");
            return;
        }
        if(!etAge.isValid() || etAge.getText().isEmpty()){
            tvError.setText("Please enter age");
            return;
        }
        name = etName.getText();
        address = etAddress.getText();

        try {
            age = Integer.parseInt(etAge.getText());
        } catch (NumberFormatException e) {
            tvError.setText("Invalid age entered...");
            return;
        }
        onSuccessListener.accept(new Customer(
                null,
                age,
                name,
                address,
                null,
                null
        ));
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }
}
