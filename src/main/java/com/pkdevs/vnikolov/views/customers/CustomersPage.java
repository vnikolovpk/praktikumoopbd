package com.pkdevs.vnikolov.views.customers;

import com.pkdevs.vnikolov.database.Database;
import com.pkdevs.vnikolov.database.models.Customer;
import com.pkdevs.vnikolov.util.PlaceholderSearchFIeld;
import com.pkdevs.vnikolov.util.TableMouseListener;
import com.pkdevs.vnikolov.views.CrudPopupMenu;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;

public class CustomersPage extends ComponentAdapter {
    private JPanel rootPanel;
    private JButton createButton;
    private JPanel tablePanel;
    private JTextField searchField;

    // Dependencies
    private final Database database;

    private CustomersTableModel customersTableModel;

    public CustomersPage(Database database) {
        this.database = database;

        rootPanel.addComponentListener(this);
        tablePanel.setLayout(new BorderLayout());

        customersTableModel = new CustomersTableModel(this::updateCustomer);

        JTable table = new JTable();

        CrudPopupMenu crudPopupMenu = new CrudPopupMenu(
                table,
                i -> deleteCustomer(customersTableModel.getCustomerAtRow(i))
        );

        table.setComponentPopupMenu(crudPopupMenu);

        table.addMouseListener(new TableMouseListener(table));

        table.setModel(customersTableModel);
        table.setFillsViewportHeight(true);

        tablePanel.add(table.getTableHeader(), BorderLayout.PAGE_START);
        tablePanel.add(table, BorderLayout.CENTER);
        createButton.addActionListener(this::createButtonClicked);
    }

    private void createButtonClicked(ActionEvent actionEvent) {
        CreateCustomerDialog dialog = new CreateCustomerDialog(this::createCustomer);
        dialog.pack();
        dialog.setVisible(true);
    }

    private void createCustomer(Customer customer) {
        database.customers.insert(customer);
        refreshCustomers();
    }

    private void updateCustomer(Integer id, Customer updates) {
        database.customers.update(id, updates);
        refreshCustomers();
    }

    public JPanel getPanel() {
        return rootPanel;
    }

    @Override
    public void componentShown(ComponentEvent componentEvent) {
        refreshCustomers();
    }

    public void refreshCustomers(){
        refreshCustomers(null);
    }
    public void refreshCustomers(String search){
        setCustomers(database.customers.list(search));
    }

    public void setCustomers(List<Customer> customers) {
        customersTableModel.setCustomers(customers);
    }

    private void deleteCustomer(Customer customer){
        database.customers.delete(customer.id);
        refreshCustomers();
    }

    private void createUIComponents() {
        searchField = new PlaceholderSearchFIeld();
        searchField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent keyEvent) {
                String text = searchField.getText();
                refreshCustomers(text.isEmpty() ? null : text);
            }
        });
    }
}
