package com.pkdevs.vnikolov.views.customers;

import com.pkdevs.vnikolov.database.models.Customer;

import javax.swing.table.AbstractTableModel;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;

public class CustomersTableModel extends AbstractTableModel {

    private static final String COLUMN_NAME = "Name";
    private static final String COLUMN_AGE = "Age";
    private static final String COLUMN_ADDRESS = "Address";

    private final List<String> columnNames = Arrays.asList(
        COLUMN_NAME, COLUMN_AGE, COLUMN_ADDRESS
    );
    private final BiConsumer<Integer, Customer> updateHandler;

    private List<Customer> customers;

    public CustomersTableModel(BiConsumer<Integer, Customer> updateHandler) {
        this.updateHandler = updateHandler;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
        fireTableDataChanged();
    }

    @Override
    public void setValueAt(Object o, int row, int col) {
        if(columnNames.indexOf(COLUMN_ADDRESS) == col) addressUpdated((String) o, customers.get(row));
        if(columnNames.indexOf(COLUMN_NAME) == col) nameUpdated((String) o, customers.get(row));
        if(columnNames.indexOf(COLUMN_AGE) == col) ageUpdated((int) o, customers.get(row));
    }

    private void nameUpdated(String newValue, Customer current){
        if(!newValue.equals(current.name)){
            updateHandler.accept(
                    current.id,
                    new Customer(
                            null,
                            null,
                            newValue,
                            null,
                            null,
                            null
                    )
            );
        }
    }

    private void ageUpdated(int newValue, Customer current){
        if(newValue != current.age){
            updateHandler.accept(
                    current.id,
                    new Customer(
                            null,
                            newValue,
                            null,
                            null,
                            null,
                            null
                    )
            );
        }
    }

    private void addressUpdated(String newValue, Customer current){
        if(!newValue.equals(current.address)){
            updateHandler.accept(
                    current.id,
                    new Customer(
                            null,
                            null,
                            null,
                            newValue,
                            null,
                            null
                    )
            );
        }
    }


    @Override
    public boolean isCellEditable(int i, int i1) {
        return true;
    }

    @Override
    public String getColumnName(int i) {
        return columnNames.get(i);
    }

    @Override
    public int getRowCount() {
        return customers != null ? customers.size() : 0;
    }

    @Override
    public int getColumnCount() {
        return columnNames.size();
    }

    @Override
    public Class<?> getColumnClass(int col) {
        if(columnNames.indexOf(COLUMN_ADDRESS) == col) return String.class;
        if(columnNames.indexOf(COLUMN_NAME) == col) return String.class;
        if(columnNames.indexOf(COLUMN_AGE) == col) return Integer.class;
        return null;
    }

    @Override
    public Object getValueAt(int row, int col) {
        if(columnNames.indexOf(COLUMN_ADDRESS) == col) return customers.get(row).address;
        if(columnNames.indexOf(COLUMN_NAME) == col) return customers.get(row).name;
        if(columnNames.indexOf(COLUMN_AGE) == col) return customers.get(row).age;
        return null;
    }

    public Customer getCustomerAtRow(int row){
        return customers.get(row);
    }
}
