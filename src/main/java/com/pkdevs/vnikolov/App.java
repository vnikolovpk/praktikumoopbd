package com.pkdevs.vnikolov;

import com.pkdevs.vnikolov.database.Database;
import com.pkdevs.vnikolov.views.MainScreen;

import javax.swing.*;

class App {
    void start() {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (ClassNotFoundException
                | InstantiationException
                | UnsupportedLookAndFeelException
                | IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        Database database = new Database();

        new MainScreen(database);

//        List<FullRepair> fullRepairs = database.multiTableRepairsSearch(null);

//        for (FullRepair fullRepair : fullRepairs) {
//            System.out.println(fullRepair.toString());
//        }

//        database.insertCustomer(new Customer(
//                null,
//                12,
//                "Gancho",
//                "perniko",
//                null,
//                null
//        ));

//        Car zdr = database.insertCar(
//                new Car(
//                        null,
//                        "Honda",
//                        "Civic",
//                        2003,
//                        1,
//                        null,
//                        null
//                )
//        );
//
//        System.out.println(zdr.toString());
    }
}
