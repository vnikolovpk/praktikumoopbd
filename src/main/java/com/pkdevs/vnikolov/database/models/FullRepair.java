package com.pkdevs.vnikolov.database.models;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FullRepair {
    public final String customer;
    public final String car;
    public final String repair;
    public final int price;

    private FullRepair(String customer, String car, String repair, int price) {
        this.customer = customer;
        this.car = car;
        this.repair = repair;
        this.price = price;
    }

    @Override
    public String toString() {
        return "FullRepair{" +
                "customer='" + customer + '\'' +
                ", car='" + car + '\'' +
                ", repair='" + repair + '\'' +
                ", price=" + price +
                '}';
    }

    public static FullRepair fromResultSet(ResultSet rs) {
        try {
            return new FullRepair(
                rs.getString("customer"),
                String.valueOf(rs.getInt("year"))
                        + " " + rs.getString("make")
                        + " " + rs.getString("model"),
                rs.getString("repair"),
                rs.getInt("price")
            );
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
