package com.pkdevs.vnikolov.database.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

public class Car implements Serializable{
    public static final String TABLE_NAME = "cars";

    public static final String[] SEARCHABLE_FIELDS = {
        "year"
    };

    public final Integer id;
    public final Integer model;
    public final Integer year;
    public final Integer owner;

    public final Timestamp createdAt;
    public final Timestamp updatedAt;

    public Car(Integer id, Integer model, Integer year, Integer owner, Timestamp createdAt, Timestamp updatedAt) {
        this.id = id;
        this.model = model;
        this.year = year;
        this.owner = owner;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public static Car fromResultSet(ResultSet rs) {
        try {
            return new Car(
                rs.getInt("id"),
                rs.getInt("model"),
                rs.getInt("year"),
                rs.getInt("owner"),
                rs.getTimestamp("created_at"),
                rs.getTimestamp("updated_at")
            );
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", model=" + model +
                ", year=" + year +
                ", owner=" + owner +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }

    public Map<String, Object> toKeyValuePairs(){
        HashMap<String, Object> result = new HashMap<>();
        if(this.year != null) result.put("year", this.year);
        if(this.owner != null) result.put("owner", this.owner);
        if(this.model != null) result.put("model", this.model);
        return result;
    }
}
