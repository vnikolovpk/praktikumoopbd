package com.pkdevs.vnikolov.database.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class Manufacturer implements Serializable{
    public static final String TABLE_NAME = "manufacturers";

    public static final String[] SEARCHABLE_FIELDS = {
            "name"
    };

    public final Integer id;
    public final String name;

    public Manufacturer(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public static Manufacturer fromResultSet(ResultSet rs) {
        try {
            return new Manufacturer(
                rs.getInt("id"),
                rs.getString("name")
            );
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public String toString() {
        return "Manufacturer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public Map<String, Object> toKeyValuePairs(){
        HashMap<String, Object> result = new HashMap<>();
        if(this.name != null) result.put("name", this.name);
        return result;
    }
}
