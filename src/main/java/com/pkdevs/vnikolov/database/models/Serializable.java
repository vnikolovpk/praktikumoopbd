package com.pkdevs.vnikolov.database.models;

import java.util.Map;

public interface Serializable {
    public Map<String, Object> toKeyValuePairs();
}
