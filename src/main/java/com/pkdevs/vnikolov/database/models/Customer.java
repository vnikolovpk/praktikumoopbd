package com.pkdevs.vnikolov.database.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

public class Customer implements Serializable{
    public static final String TABLE_NAME = "customers";

    public static final String[] SEARCHABLE_FIELDS = {
        "age", "name", "address"
    };

    public final Integer id;
    public final Integer age;
    public final String name;
    public final String address;

    public final Timestamp createdAt;
    public final Timestamp updatedAt;

    public Customer(Integer id, Integer age, String name, String address, Timestamp createdAt, Timestamp updatedAt) {
        this.id = id;
        this.age = age;
        this.name = name;
        this.address = address;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public static Customer fromResultSet(ResultSet rs) {
        try {
            return new Customer(
                rs.getInt("id"),
                rs.getInt("age"),
                rs.getString("name"),
                rs.getString("address"),
                rs.getTimestamp("created_at"),
                rs.getTimestamp("updated_at")
            );
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", age=" + age +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }

    public Map<String, Object> toKeyValuePairs(){
        HashMap<String, Object> result = new HashMap<>();
        if(this.id != null) result.put("id", this.id);
        if(this.age != null) result.put("age", this.age);
        if(this.name != null) result.put("name", this.name);
        if(this.address != null) result.put("address", this.address);
        return result;
    }
}
