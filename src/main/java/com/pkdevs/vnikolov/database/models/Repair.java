package com.pkdevs.vnikolov.database.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

public class Repair implements Serializable {
    public static final String TABLE_NAME = "repairs";

    public static final String[] SEARCHABLE_FIELDS = {
        "description", "price"
    };
    public final Integer id;
    public final String description;
    public final Integer price;
    public final Integer car;

    public final Timestamp createdAt;
    public final Timestamp updatedAt;

    public Repair(Integer id, String description, Integer price, Integer car, Timestamp createdAt, Timestamp updatedAt) {
        this.id = id;
        this.description = description;
        this.price = price;
        this.car = car;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public static Repair fromResultSet(ResultSet rs) {
        try {
            return new Repair(
                    rs.getInt("id"),
                    rs.getString("description"),
                    rs.getInt("price"),
                    rs.getInt("car"),
                    rs.getTimestamp("created_at"),
                    rs.getTimestamp("updated_at")
            );
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public String toString() {
        return "Repair{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", car=" + car +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }

    public Map<String, Object> toKeyValuePairs(){
        HashMap<String, Object> result = new HashMap<>();
        if(this.price != null) result.put("price", this.price);
        if(this.description != null) result.put("description", this.description);
        if(this.car != null) result.put("car", this.car);
        return result;
    }
}
