package com.pkdevs.vnikolov.database.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class Model implements Serializable{

    public static final String TABLE_NAME = "models";

    public static final String[] SEARCHABLE_FIELDS = {
        "name"
    };

    public final Integer id;
    public final Integer manufacturer;
    public final String name;

    public Model(Integer id, String name, Integer manufacturer) {
        this.id = id;
        this.name = name;
        this.manufacturer = manufacturer;
    }

    public static Model fromResultSet(ResultSet rs) {
        try {
            return new Model(
                    rs.getInt("id"),
                    rs.getString("name"),
                    rs.getInt("manufacturer")
            );
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }


    public Map<String, Object> toKeyValuePairs(){
        HashMap<String, Object> result = new HashMap<>();
        if(this.name != null) result.put("name", this.name);
        if(this.manufacturer != null) result.put("manufacturer", this.manufacturer);
        return result;
    }
}
