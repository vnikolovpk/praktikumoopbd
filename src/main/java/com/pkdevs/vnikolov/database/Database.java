package com.pkdevs.vnikolov.database;


import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import com.pkdevs.vnikolov.database.models.*;
import com.pkdevs.vnikolov.util.PropertiesUtil;
import com.pkdevs.vnikolov.util.QueryUtil;
import com.pkdevs.vnikolov.util.ResourceUtil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

@SuppressWarnings({"UnusedReturnValue", "WeakerAccess"})
public class Database {

    private final MysqlDataSource dataSource;

    public final GenericDatabaseCRUD<Repair> repairs;
    public final GenericDatabaseCRUD<Car> cars;
    public final GenericDatabaseCRUD<Customer> customers;
    public final GenericDatabaseCRUD<Manufacturer> manufacturers;
    public final GenericDatabaseCRUD<Model> models;

    public Database() {
        try {
            Properties props = PropertiesUtil.loadProperties("dbconfig.properties");

            dataSource = makeDefaultDataSource(
                    props.getProperty("host"),
                    Integer.valueOf(props.getProperty("port")),
                    props.getProperty("username"),
                    props.getProperty("password"),
                    props.getProperty("database")
            );

            createSchemaIfNotExists();

            repairs = new GenericDatabaseCRUD<>(
                    dataSource,
                    Repair.TABLE_NAME,
                    Repair::fromResultSet,
                    Repair.SEARCHABLE_FIELDS
            );

            cars = new GenericDatabaseCRUD<>(
                    dataSource,
                    Car.TABLE_NAME,
                    Car::fromResultSet,
                    Car.SEARCHABLE_FIELDS
            );

            customers = new GenericDatabaseCRUD<>(
                    dataSource,
                    Customer.TABLE_NAME,
                    Customer::fromResultSet,
                    Customer.SEARCHABLE_FIELDS
            );

            manufacturers = new GenericDatabaseCRUD<>(
                    dataSource,
                    Manufacturer.TABLE_NAME,
                    Manufacturer::fromResultSet,
                    Manufacturer.SEARCHABLE_FIELDS
            );

            models = new GenericDatabaseCRUD<>(
                    dataSource,
                    Model.TABLE_NAME,
                    Model::fromResultSet,
                    Model.SEARCHABLE_FIELDS
            );


        } catch (IOException | SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private MysqlDataSource makeDefaultDataSource(String host,
                                                  int port,
                                                  String username,
                                                  String password,
                                                  String database) {
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setServerName(host);
        dataSource.setPort(port);

        dataSource.setUser(username);
        dataSource.setPassword(password);

        dataSource.setDatabaseName(database);
        return dataSource;
    }

    private void createSchemaIfNotExists() throws SQLException, IOException {
        Connection conn = dataSource.getConnection();
        String sql = ResourceUtil.getResourceFileAsString("create-tables.sql");
        Statement stmt = conn.createStatement();
        if (sql == null) throw new RuntimeException("Bad 'create-tables.sql' resource");
        for (String query : sql.split(";")) {
            stmt.addBatch(query);
        }
        stmt.executeBatch();
        conn.close();
    }

    public List<FullRepair> multiTableRepairsSearch(
            Map<String, String> filters
    ){
        try {
            List<FullRepair> results = new ArrayList<>();
            Connection conn = dataSource.getConnection();
            Statement stmt = conn.createStatement();
            StringBuilder sqlBuilder = new StringBuilder();
            sqlBuilder.append("SELECT");
            sqlBuilder.append(" customers.name AS customer,");
            sqlBuilder.append(" cars.year AS year,");
            sqlBuilder.append(" manufacturers.name AS make,");
            sqlBuilder.append(" models.name AS model,");
            sqlBuilder.append(" repairs.description AS repair,");
            sqlBuilder.append(" repairs.price AS price");
            sqlBuilder.append(" FROM repairs");
            sqlBuilder.append(" INNER JOIN cars ON repairs.car=cars.id");
            sqlBuilder.append(" INNER JOIN customers ON cars.owner=customers.id");
            sqlBuilder.append(" INNER JOIN models ON cars.model=models.id");
            sqlBuilder.append(" INNER JOIN manufacturers ON models.manufacturer=manufacturers.id");
            if(filters != null && filters.size() > 0){
                sqlBuilder.append(" WHERE ").append(
                    String.join(" AND ", filters
                        .entrySet()
                        .stream()
                        .filter(Objects::nonNull)
                        .map(QueryUtil::paramToLike)
                        .toArray(String[]::new))
                );
            }

            String s = sqlBuilder.toString();

//            System.out.println(s);

            stmt.executeQuery(s);
            ResultSet rs = stmt.getResultSet();
            while (rs.next()) {
                results.add(FullRepair.fromResultSet(rs));
            }
            rs.close();
            stmt.close();
            conn.close();
            return results;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
