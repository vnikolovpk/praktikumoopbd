package com.pkdevs.vnikolov.database;

import com.pkdevs.vnikolov.database.models.Serializable;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * This class is my baby, I am fukin proud of it, ok!?
 *
 * @author Veselin Nikolov /vnikolov@pkdevs.com/
 * @since 02.09.2020
 */
public class GenericDatabaseCRUD<T extends Serializable> {

    private final DataSource dataSource;
    private final Function<ResultSet, T> parser;
    private final String tableName;
    private final String[] searchableFields;

    public GenericDatabaseCRUD(DataSource dataSource,
                               String tableName,
                               Function<ResultSet, T> parser,
                               String[] searchableFields) {
        this.dataSource = dataSource;
        this.tableName = tableName;
        this.searchableFields = searchableFields;
        this.parser = parser;
    }

    public List<T> list(){
        return list(null);
    }

    public List<T> list(String query) {
        try {
            List<T> results = new ArrayList<>();
            Connection conn = dataSource.getConnection();
            Statement stmt = conn.createStatement();
            String sql = "SELECT * FROM " + tableName;
            if (query != null) {
                // Should I bind these params - probably. Will I bind them - nope.
                sql += " WHERE " + String.join(
                        " OR ",
                        Stream.of(searchableFields)
                                .map(fName -> fName + " LIKE '%" + query + "%'")
                                .toArray(String[]::new)
                );
            }
            stmt.executeQuery(sql);
            ResultSet rs = stmt.getResultSet();
            while (rs.next()) {
                results.add(parser.apply(rs));
            }
            rs.close();
            stmt.close();
            conn.close();
            return results;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public T get(int id) {
        try {
            T result = null;
            Connection conn = dataSource.getConnection();
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM " + tableName + " WHERE id=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                result = parser.apply(rs);
                if (rs.next()) throw new RuntimeException("Multiple " + tableName + " with id " + id + " found");
            }
            rs.close();
            stmt.close();
            conn.close();
            return result;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void delete(int id) {
        try {
            System.out.println("Deleting "+tableName+": "+id);
            Connection conn = dataSource.getConnection();
            PreparedStatement stmt = conn.prepareStatement("DELETE FROM " + tableName + " WHERE id=?");
            stmt.setInt(1, id);
            stmt.executeUpdate();
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public T insert(T data) {
        try {
            Map<String, Object> fields = data.toKeyValuePairs();

            System.out.println("Inserting "+tableName+": "+fields.toString());

            String sql = "INSERT INTO "+tableName;

            // Field names
            sql += "("+String.join(",", fields.keySet())+")";

            // Probably the most complicated way to generate a set number of question marks
            sql += " VALUES ("+String.join(",", IntStream.range(0, fields.size())
                    .mapToObj(ignored -> "?")
                    .toArray(String[]::new))+")";

            Connection conn = dataSource.getConnection();
            PreparedStatement stmt = conn.prepareStatement(sql);

            int i = 1;
            for (Map.Entry<String, Object> kv : fields.entrySet()) {
                if(kv.getValue() instanceof String) stmt.setString(i, (String) kv.getValue());
                else if(kv.getValue() instanceof Integer) stmt.setInt(i, (Integer) kv.getValue());
                else throw new IllegalArgumentException("Unknown type of argument for insert.");
                i++;
            }

            int affectedRows = stmt.executeUpdate();

            if (affectedRows == 0) throw new SQLException("Creating customer failed, no rows affected.");

            ResultSet generatedKeys = stmt.getGeneratedKeys();
            generatedKeys.next();
            int insertId = generatedKeys.getInt(1);

            generatedKeys.close();
            stmt.close();
            conn.close();
            return get(insertId);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public T update(int id, T data) {
        try {
            Map<String, Object> updates = data.toKeyValuePairs();
            System.out.println("Updating "+tableName+": "+updates.toString());
            Connection conn = dataSource.getConnection();

            StringBuilder setsBuilder = new StringBuilder();
            if(updates.size() < 1) throw new IllegalArgumentException("No updates to apply");

            for (Map.Entry<String, Object> kv : updates.entrySet()) {
                if(kv.getValue() instanceof String){
                    setsBuilder.append(" ").append(kv.getKey()).append("='").append((String) kv.getValue()).append("'");
                } else if(kv.getValue() instanceof Integer){
                    setsBuilder.append(" ").append(kv.getKey()).append("=").append(String.valueOf(kv.getValue()));
                } else throw new IllegalArgumentException("Unknown type of argument for update.");
            }

            if (setsBuilder.length() < 1) return null;

            Statement stmt = conn.createStatement();

            String sqlBuilder = "UPDATE "+tableName+" SET" + setsBuilder + " WHERE id=" + id;

            int affectedRows = stmt.executeUpdate(sqlBuilder);

            if (affectedRows == 0) return null;

            stmt.close();
            conn.close();
            return get(id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
