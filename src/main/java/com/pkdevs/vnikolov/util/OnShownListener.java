package com.pkdevs.vnikolov.util;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.function.Consumer;

// Wrote this then decided not to use it because it would create too many unnecessary objects.
// Leaving it here if I ever decide to use it tho
public class OnShownListener extends ComponentAdapter {

    private final Consumer<ComponentEvent> callback;

    public OnShownListener(Consumer<ComponentEvent> callback) {
        this.callback = callback;
    }

    @Override
    public void componentShown(ComponentEvent componentEvent) {
        callback.accept(componentEvent);
    }
}
