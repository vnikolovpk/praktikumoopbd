package com.pkdevs.vnikolov.util;

import java.util.Map;

public class QueryUtil {
    public static String paramToLike(Map.Entry<String, String> i){
        return i.getKey().equals("models.name") ? "(models.name LIKE '%"+i.getValue() +"%'" +
                " OR manufacturers.name LIKE '%"+i.getValue()+"%'" +
                " OR cars.year LIKE '%"+i.getValue()+"%')" : i.getKey() + " LIKE '%" + i.getValue() + "%'";
    }
}
