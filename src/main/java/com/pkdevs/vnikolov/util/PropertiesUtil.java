package com.pkdevs.vnikolov.util;

import java.io.*;
import java.util.Properties;

public class PropertiesUtil {
    public static Properties loadProperties(String filePath) throws IOException {
        Properties prop = new Properties();
        prop.load(new FileInputStream(filePath));
        return prop;
    }
}
