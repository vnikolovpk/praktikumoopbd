package com.pkdevs.vnikolov.util;

import com.pkdevs.vnikolov.database.models.Car;
import com.pkdevs.vnikolov.database.models.Manufacturer;
import com.pkdevs.vnikolov.database.models.Model;

import java.util.List;

public class Relator {
    public static String makeModelForCar(List<Manufacturer> manufacturers, List<Model> models, Car car){
        Model model = models.stream()
                .filter(i -> i.id.equals(car.model))
                .findFirst()
                .orElse(null);
        if(model == null) return "Unknown model";
        Manufacturer make = manufacturers
                .stream()
                .filter(i -> i.id.equals(model.manufacturer))
                .findFirst()
                .orElse(null);
        if(make == null) return "Unknown make";
        return String.valueOf(car.year) + " " + make.name + " " + model.name;
    }
}
